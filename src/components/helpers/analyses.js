module.exports = [
	{
	  NAME: 'Analysis 1 (name: ANA1)',//PHENOTYPE NAME
	  IC: 'SARS-CoV-2 infected individuals with a clinical window of 15-90 days from diagnosis *. ', //INCLUSION CRITERIA
	  CASES: 'death OR respiratory support (intubation, CPAP, CPN or BiPAP, See Appendix 1 for codes).',
	  CONTROLS: 'alive individuals without respiratory support at any point since diagnosis.',
	},
	{
	  NAME: 'Analysis 2 (name: ANA2)',//PHENOTYPE NAME
	  IC: 'SARS-CoV-2 infected individuals with a clinical window of 15-90 days from diagnosis *.', //INCLUSION CRITERIA
	  CASES: 'hospitalized (in-patient) individuals',
	  CONTROLS: 'non-hospitalized individuals',
	},
	{
	  NAME: 'Analysis 3-mortality (phenotype name: ANA3)',//PHENOTYPE NAME
	  IC: 'SARS-CoV-2 infected individuals with a clinical window of 15-90 days from diagnosis *.', //INCLUSION CRITERIA
	  CASES: 'individuals that died',
	  CONTROLS: 'alive individuals',
	},
	{
	  NAME: 'Analysis 4-severity ordinal  (phenotype name: ANA4)',//PHENOTYPE NAME
	  IC: 'hospitalized SARS-CoV-2 infected individuals with a clinical window of 15-90 days from diagnosis *.', //INCLUSION CRITERIA
	  CASES: '3 level scale (mild,severe,critical) of COVID19 disease severity based on https://jamanetwork.com/journals/jama/fullarticle/2762130. Mild: nonpneumonia and mild pneumonia. Severe: dyspnea, respiratory frequency ≥30/min, blood oxygen saturation ≤93%, partial pressure of arterial oxygen to fraction of inspired oxygen ratio <300, and/or lung infiltrates >50% within 24 to 48 hours. Critical: respiratory failure, septic shock, and/or multiple organ dysfunction or failure. Adaptations of this scale are possible within each study. Studies will need to report the exact criteria used to define: mild,severe,critical.',
	  CONTROLS: 'Group study into [case/control] mild/severe, severe/critical, critical/mild ',
	},
	{
	  NAME: 'Analysis 5-susceptibility  (phenotype name: ANA5)',//PHENOTYPE NAME
	  IC: 'everyone.', //INCLUSION CRITERIA
	  CASES: 'individuals diagnosed with COVID-19.',
	  CONTROLS: 'individuals without documented diagnosis of COVID-19.',
	},
	{
	  NAME: 'Analysis 6-susceptibility-severe  (phenotype name: ANA6)',//PHENOTYPE NAME
	  IC: 'everyone.', //INCLUSION CRITERIA
	  CASES: 'individuals diagnosed with COVID-19 and hospitalized.',
	  CONTROLS: 'individuals without documented diagnosis of COVID-19.',
	},
	{
	  NAME: 'Analysis 7-flu symptoms  (phenotype name: ANA7)',//PHENOTYPE NAME
	  IC: ' everyone.', //INCLUSION CRITERIA
	  CASES: 'individuals reporting flu-related symptoms (definition is cohort-specific and based on questionnaire data, see Appendix 2 for an example) during the COVID-19 pandemic period. ',
	  CONTROLS: 'individuals reporting no flu-related symptoms',
	},
]