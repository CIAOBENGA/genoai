import Vue from 'vue'
import Router from 'vue-router'
import Messages from '@/components/Messages'
import Prelaunch from '@/components/Prelaunch'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Prelaunch
    }, 
    {
      path: '/messages',
      name: 'messages',
      component: Messages
    },

  ]
})
