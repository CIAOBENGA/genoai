import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import BootstrapVue from 'bootstrap-vue'
import store from './store'
import router from '@/router'
import vuetify from '@/plugins/vuetify' // path to vuetify export

Vue.config.productionTip = false
// ./src/main.js


Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// font awesome settings
import { library } from '@fortawesome/fontawesome-svg-core'
import {
        faFilePdf,
        faFileImage,
        faFileExcel,
        faFilePowerpoint,
        faFileWord,
        faFileVideo,
        faFileArchive,
        faFileAlt,
        faFile,
        faTrashAlt,
        faUpload,
        faTasks
       } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
            faFilePdf,
            faFileImage,
            faFileExcel,
            faFilePowerpoint,
            faFileWord,
            faFileVideo,
            faFileArchive,
            faFileAlt,
            faFile,
            faTrashAlt,
            faUpload,
            faTasks
          )

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

// axios settings
// axios.defaults.baseURL = ''
// axios.defaults.xsrfHeaderName = "X-CSRFToken"
// axios.defaults.xsrfCookieName = 'csrftoken'
// axios.defaults.headers.common['Authorization'] = 'AUTH_TOKEN';

// myAxios.js
const instance = axios.create({
  baseURL: 'http://localhost:8000',
  headers: {
    common: {
      Authorization: 'AUTH_TOKEN_FROM_INSTANCE'
    }
  }
});
// Vue.use(VueRouter)
 
const vue = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}) 

vue.$mount('#app')
