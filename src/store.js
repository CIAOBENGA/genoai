// ./src/store.js

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex, axios)

export default new Vuex.Store ({
  state: {
    rowData: [],
    studyData: [],
  },

  actions: {
    loadFiles ({ commit }) {
      axios.get('api/documents/').then(data => {
        let rowData = data.data
        commit('SET_FILES', rowData)
      }).catch(error => {
        console.log(error) 
      })
    },

    getStudyData({commit}) {
      axios.get('api/studyData/').then(data => {
        let studyData = data.data
        commit('SET_STUDYDATA', studyData)
      }).catch(error => {
        console.log(error)
      })
    },

    postFile ({ dispatch, commit }, newFile) {
      const config = {
        onUploadProgress (e) {
          var percentCompleted = Math.round( (e.loaded * 5000) / e.total );
          console.log(percentCompleted)
        }
      };
      try {
        axios.post('api/documents/', newFile, config,
        { headers: {
          'Content-Type': 'multipart/form-data'
           }
        })
          .then(res => {
            commit('POST_FILE', newFile)
            console.log(res)
          })
          .then(res => {
            dispatch('loadFiles')
            console.log(res)
          })
      } catch (error) {
        console.log(error);
      }
    },

    deleteFile ({ dispatch }, result_id) {
      axios.delete('api/files/' + result_id)
        .then(res => {
          dispatch('loadFiles')
          console.log(res)
        })
        .catch(error => {
          console.log(error)
      })
    },

    downloadFile ({ dispatch }, filename) {
      axios({
        url: `media/${filename}`,
        method: 'GET',
        responseType: 'blob',
      })
      .then ((res) => {
        const url = window.URL.createObjectURL(new Blob([res.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', filename)
        document.body.appendChild(link)
        link.click()
        console.log(res)
        console.log(dispatch)
      })
    }
  },

  mutations: {
    SET_FILES (state, files) {
      state.rowData = files
    },
    POST_FILE (state, newFile) {
      state.rowData.push(newFile)
    },
    SET_STUDYDATA(state, studyData) {
      state.studyData = studyData
    }
  }
})