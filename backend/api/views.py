from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework import viewsets
from .serializers import DataSerializer
from .models import Message, MessageSerializer, Data, Document, DocumentSerializer, StudyData, StudyDataSerializer
from django.shortcuts import get_object_or_404
from django.http import Http404
from django import forms

from rest_framework.response import Response # from viewsets doc
from rest_framework import permissions, status
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from rest_framework.views import APIView

import cloudinary.uploader



# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))


class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class DataViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer 

    permission_classes = (permissions.AllowAny,) # we assume that we have a session user
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, format=None):
        serializer = DataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def destroy(self, request, *args, **kwargs):
        try:
            for k, v in kwargs.items():
                for id in v.split(','):
                    obj = get_object_or_404(Data, pk=int(id))
                    self.perform_destroy(obj)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class DocumentViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer

    def post(request):
        serializer = DocumentSerializer(request.filename, request.file)
        if serializer.is_valid():
            serializer.save()   
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudyDataViewSet(viewsets.ModelViewSet):
    queryset = StudyData.objects.order_by('-created_at')
    serializer_class = StudyDataSerializer

    def post(request):
        serializer = StudyDataSerializer(request.investigator, request.country, request.studyName, request.studyDescription, request.analysis, request.affiliation, request.studyUUID)
        if serializer.is_valid():
            serializer.save()   
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)