from django.db import models
from rest_framework import serializers
from django.utils import timezone
from s3direct.fields import S3DirectField
from .storage_backends import PublicMediaStorage

class Message(models.Model):
    subject = models.CharField(max_length=200)
    body = models.TextField()
 

class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ('url', 'subject', 'body', 'pk')

class Data(models.Model):
    file_id = models.AutoField(primary_key=True)
    file = models.FileField(null=True, max_length=255)
    date_created = models.DateTimeField(default = timezone.now)
    def __str__(self):
        return str(self.file.name)

class PlinkData(models.Model):
    file_id = models.AutoField(primary_key=True)
    file = S3DirectField(dest='primary_destination', blank=True)
    date_created = models.DateTimeField(default = timezone.now)

    def __str__(self):
        return str(self.file.name)

class Document(models.Model):
    file_id = models.AutoField(primary_key=True)
    filename = models.CharField(max_length=200)
    file = models.FileField(storage=PublicMediaStorage())
    uploaded_at = models.DateTimeField(auto_now_add=True)


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ('filename', 'file', )


class StudyData(models.Model):
    study_id = models.AutoField(primary_key=True)
    investigator = models.CharField(max_length=400)
    country = models.CharField(max_length=400)
    studyName = models.CharField(max_length=400)
    studyDescription = models.CharField(max_length=400)
    analysis = models.CharField(max_length=400)
    affiliation = models.CharField(max_length=400)
    studyUUID = models.CharField(max_length=400)
    created_at = models.DateTimeField(auto_now_add=True)


class StudyDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudyData
        fields = ('investigator','country','studyName','studyDescription','analysis','affiliation','studyUUID')