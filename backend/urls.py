"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from django.conf import settings                      # add this 
from django.conf.urls.static import static            # add this	
from .api.views import index_view, MessageViewSet, DataViewSet, DocumentViewSet, StudyDataViewSet


router = routers.DefaultRouter()
router.register('messages', MessageViewSet)
router.register('documents', DocumentViewSet)
router.register('files', DataViewSet, basename='data')
router.register('studyData', StudyDataViewSet)


urlpatterns = [ 

    # http://localhost:8000/
    path('', index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),

    # http://localhost:8000/api/admin/
    path('api/admin/', admin.site.urls), 

    path(r's3direct/', include('s3direct.urls')),

]

if settings.DEBUG:     
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)   
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

